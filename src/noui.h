// Copyright (c) 2013 The Aureus developers
// Distributed under the MIT/X11 software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef AUREUS_NOUI_H
#define AUREUS_NOUI_H

extern void noui_connect();

#endif // AUREUS_NOUI_H

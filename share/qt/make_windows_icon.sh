#!/bin/bash
# create multiresolution windows icon
ICON_SRC=../../src/qt/res/icons/aureus.png
ICON_DST=../../src/qt/res/icons/aureus.ico
convert ${ICON_SRC} -resize 16x16 aureus-16.png
convert ${ICON_SRC} -resize 32x32 aureus-32.png
convert ${ICON_SRC} -resize 48x48 aureus-48.png
convert aureus-16.png aureus-32.png aureus-48.png ${ICON_DST}


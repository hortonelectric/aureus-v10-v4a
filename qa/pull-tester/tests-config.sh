#!/bin/bash
# Copyright (c) 2013-2014 The Aureus Core developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

BUILDDIR="/home/ubuntu/aureus-0.10-v4A"
EXEEXT=""

# These will turn into comments if they were disabled when configuring.
ENABLE_WALLET=1
ENABLE_UTILS=1
ENABLE_AUREUSD=1

REAL_AUREUSD="$BUILDDIR/src/aureusd${EXEEXT}"
REAL_AUREUSCLI="$BUILDDIR/src/aureus-cli${EXEEXT}"

